import cv2
import glob

images = [cv2.imread(image, 0) for image in glob.glob("frames/*.jpg")[:500]]
# images = images[:500]
result = cv2.bitwise_and(images[0], images[1])
for index, image in enumerate(images[2:]):
    print(index)
    result = cv2.bitwise_and(result, image)
    cv2.imshow('result', result)
    cv2.waitKey(0)
cv2.imwrite("result.png", result)
cv2.imshow('result', result)
cv2.waitKey(0)
